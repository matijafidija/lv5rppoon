﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class ShippingService
    {
        private double Postarina;

        public ShippingService(double Shipment) 
        {
            this.Postarina = Shipment;
        }

        public double Price(double weight)
        {
            return Postarina * weight;
        }

        public override string ToString()
        {
            return "Cijena dostave: ";
        }

    }
}
