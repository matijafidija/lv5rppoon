﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> list = new List<IShipable>();
            
            Product prod1 = new Product("Adam audio T7V", 1800, 8.2);
            Product prod2 = new Product("AKG C414 XLII", 5766.31, 1.4);
            Box box = new Box("Studio posiljka");

            list.Add(box);
            list.Add(prod1);
            list.Add(prod2);

            ShippingService price = new ShippingService(14);
            double var = 0;

            foreach (IShipable ship in list)
            {

                var += ship.Weight;

                Console.WriteLine(ship.Description());

            }

            Console.WriteLine(price.ToString() + price.Price(var) + " HRK");
        }
    }
}
